<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Comment;
use App\Image;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getCommentsPost($idPost){
        $post = Post::find($idPost);
        $comments_post = array();

        foreach ($post->comments as $comment) {
            array_push($comments_post, $comment);
        }
        return $comments_post;
    }

    public function getCommentsImage($idImage){
        $image = Image::find($idImage);
        $comments_image = array();

        foreach ($image->comments as $comment) {
            array_push($comments_image, $comment);
        }
        return $comments_image;
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $comment = new Comment();
        $comment->body = $request->body;
        $comment->commentable_id = 1;
        $comment->commentable_type = 'App\Post';
        $comment->save();
        $datos = request()->all();
        return redirect('/home');
    }

    

    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $comment = Comment::findOrFaile($request->id);
        $comment->body = $request->body;
        $comment->commentable_id = $request->commentable_id;
        $comment->commentable_type = $request->commentable_type;
        $comment->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
