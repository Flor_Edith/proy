<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Comment;
use App\Image;
use App\Post;

class Comment extends Model
{
    protected $fillable = ['body','commentable_id','commentable_type'];

    public function commentable()
    {
        return $this->morphTo();
    }
}
