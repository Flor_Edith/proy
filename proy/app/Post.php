<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Comment;
use App\Image;
use App\Post;

class Post extends Model
{
    protected $filable = ['title','body','user_id'];

    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }
    public function user(){
        return $this->belongsTo('App\User');
    }
}
