<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/main', function (){
    return view('main');
});

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/post', 'PostController@index');
Route::post('/post/registrar','PostController@store');
Route::put('/post/modificar','PostController@update');

Route::get('/comment/post/{idPost}', 'CommentController@getCommentsPost');
Route::get('/comment/image/{idImage}', 'CommentController@getCommentsImage');
Route::post('/comment/store','CommentController@store');
Route::put('/comment/update','CommentController@update');



