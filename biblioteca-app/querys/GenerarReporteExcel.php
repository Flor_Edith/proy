<?php 
require_once('../querys/ConsultarLibrosController.php'); 
require_once('../Classes/PHPExcel.php');
    $objPHPExcel = new PHPExcel();
    // Propiedades del documento
    $objPHPExcel->getProperties()->setCreator("Obed Alvarado")
    ->setLastModifiedBy("Obed Alvarado")
    ->setTitle("Office 2010 XLSX Documento de prueba")
    ->setSubject("Office 2010 XLSX Documento de prueba")
    ->setDescription("Documento de prueba para Office 2010 XLSX, generado usando clases de PHP.")
    ->setKeywords("office 2010 openxml php")
    ->setCategory("Archivo con resultado de prueba");
    // Combino las celdas desde A1 hasta E1
    $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:C2');

    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'REPORTE DE LIBROS')
                ->setCellValue('A2', 'NOMBRE')
                ->setCellValue('B2', 'ISBN')
                ->setCellValue('C2', 'ESTADO');
                
    // Fuente de la primera fila en negrita
    $boldArray = array('font' => array('bold' => true,),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));

    $objPHPExcel->getActiveSheet()->getStyle('A1:C2')->applyFromArray($boldArray);
    //Ancho de las columnas
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);	
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);	
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
    $cel=3;//Numero de fila donde empezara a crear  el reporte
    foreach ($libros as $key => $libro) {
        
			$a="A".$cel;
			$b="B".$cel;
			$c="C".$cel;
			// Agregar datos
            if($libro['estado']==1){
                $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue($a, $libro['nombre'])
            ->setCellValue($b, $libro['isbn'])
            ->setCellValue($c, 'Activo');
            $cel+=1;

            }else{
                $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue($a, $libro['nombre'])
            ->setCellValue($b, $libro['isbn'])
            ->setCellValue($c, 'Desactivado');
            $cel+=1;

            }
			
    }

    $rango="A2:$c";
    $styleArray = array('font' => array( 'name' => 'Arial','size' => 10),
    'borders'=>array('allborders'=>array('style'=> PHPExcel_Style_Border::BORDER_THIN,'color'=>array('argb' => 'FFF')))
    );
    $objPHPExcel->getActiveSheet()->getStyle($rango)->applyFromArray($styleArray);
    // Cambiar el nombre de hoja de cálculo
    $objPHPExcel->getActiveSheet()->setTitle('Reporte de libros');


    // Establecer índice de hoja activa a la primera hoja , por lo que Excel abre esto como la primera hoja
    $objPHPExcel->setActiveSheetIndex(0);
    // Redirigir la salida al navegador web de un cliente ( Excel5 )
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="reporte.xlsx"');
    header('Cache-Control: max-age=0');
    // Si usted está sirviendo a IE 9 , a continuación, puede ser necesaria la siguiente
    header('Cache-Control: max-age=1');
    
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    exit;
?>