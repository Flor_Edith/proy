<?php require_once('conn.php'); ?>
<?php 

if ($conn) {

    $sql       = "SELECT * FROM libros";

    $resultado = mysqli_query($conn, $sql);
    if (!$resultado) {
        echo "Error de BD, no se pudo consultar la base de datos\n";
        echo "Error MySQL: ". mysql_error();
        exit;
    }
    $libros = array();
    while ($fila = mysqli_fetch_assoc($resultado)) {
        $row = [
            'id' => $fila['id'],
            'nombre' => $fila['nombre'],
            'isbn' => $fila['ISBN'],
            'estado' => $fila['estado']
        ];
        array_push($libros,$row);
    }
    
    mysqli_free_result($resultado);
}

mysqli_close($conn);

?>