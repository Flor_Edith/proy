<?php require_once('../querys/ConsultarLibrosController.php'); 
    require_once('../pdf/fpdf.php');

    class PDF extends FPDF

    {

        // Page header

        function Header()

        {

            // Logo

            //$this->Image('logo.png',10,6,30);

            // Arial bold 15

            $this->SetFont('Arial','B',15);

            // Move to the right

            $this->Cell(60);

            // Title

            $this->Cell(70,10,'Reporte de libros',0,0,'C');

            // Line break

            $this->Ln(20);

        }



        // Page footer

        function Footer()

        {

            // Position at 1.5 cm from bottom

            $this->SetY(-15);

            // Arial italic 8

            $this->SetFont('Arial','I',8);

            // Page number

            $this->Cell(0,10,'Pagina '.$this->PageNo().' ',0,0,'C');

        }

    }


    $pdf = new PDF();
    $pdf->AddPage();
    $pdf->SetFont('Arial','',16);
    
    foreach ($libros as $key => $libro) { 
        $pdf->cell(80,10,$libro['nombre'],1,0,'c',0);
        $pdf->cell(50,10,$libro['isbn'],1,0,'c',0);
        if ($libro['estado'] == 1)
            $pdf->cell(60,10,'Activo',1,1,'c',0);
        else if ($libro['estado'] == 0)
            $pdf->cell(60,10,'Inactivo',1,1,'c',0);
    }

    $pdf->Output();
    //$pdf->Output("nombre_archivo.pdf","F");

?>
