<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login - Sistema de biblioteca en línea</title>
    <link rel="stylesheet" href="/biblioteca-app/css/style.css">
    <link rel="stylesheet" href="/biblioteca-app/css/normalize.css">
</head>
<body>
<ul>
            <div class="container">
            <li><a href="/biblioteca-app/Views/registrar.php">Registrarme</a></li>
            <li><a href="/biblioteca-app/Views/login.php" >Iniciar sesión</a></li>
            <li><a class="active" href="/biblioteca-app/index.php">Inicio</a></li>
            </div>
        </ul>

<div >
    <div class="container">
        <section>
    <h1 class="text-center">Ingresa tus datos</h1>
        <div class="center">
        
        <form action="/biblioteca-app/querys/LoginController.php" method="POST">
           
            <div class="form-group">
                <input type="text" name="nombre" class="form-control" placeholder="Nombre">
            </div>
            <div class="form-group">
                <input type="password" name="passwd" class="form-control" placeholder="contraseña">
            </div>
            <div class="form-group">
                <input type="submit" class="form-control btn" value="Entrar" />
            </div> 
        </form>
        </div>
        </section>
    
    </div>
</div>
    
</body>
</html>
