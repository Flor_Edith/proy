<?php 
    session_start();
    if ( $_SESSION['usuario'] == null ||  $_SESSION['usuario'] == ''){
        echo "usted no tiene autorización";
        die();
    }
    require_once('../querys/ConsultarLibrosController.php');
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Principal - Sistema de biblioteca en línea</title>
    <link rel="stylesheet" href="/biblioteca-app/css/style.css">
    <link rel="stylesheet" href="/biblioteca-app/css/normalize.css">
</head>
<body>

    <ul>
    <div class="container">
    <li><a href="/biblioteca-app/querys/CerrarSesion.php">Cerrar sesión</a></li>
    <li><a href="/biblioteca-app/querys/GenerarReportePDF.php" target="_blank">Reporte PDF</a></li>
    <li><a href="/biblioteca-app/querys/GenerarReporteExcel.php" target="_blank">Reporte Excel</a></li>
    <li><a href="/biblioteca-app/Views/libro.php" target="_blank">Nuevo libro</a></li>
    <li><a class="active" href="/biblioteca-app/Views/principal.php">Inicio</a></li>
    </div>
    </ul>



<div class="container">

<h1>Principal</h1>
<h2>Bienvenido: <?php echo $_SESSION['usuario'] ?></h2>


<h3>Aqui se muestran todos los libros registrados</h3>





<?php
    echo "<table>";
    echo "<thead>";
    echo "<tr>";
    echo "<td>Acción</td>";
    echo "<td>Nombre</td>";
    echo "<td>ISBN</td>";
    echo "<td>Estado</td>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    foreach ($libros as $libro){
        echo "<tr>";
        $url = "/biblioteca-app/Views/ModificarLibro.php?id=".$libro['id']."&nombre=".$libro['nombre']."&isbn=".$libro['isbn'];
        $eliminar = "/biblioteca-app/querys/EliminarLibroController.php?id=".$libro['id'];
        $activar = "/biblioteca-app/querys/ActivarLibroController.php?id=".$libro['id'];
        echo  "<td><a href='$url'>Modificar</a>&nbsp;";
        if($libro['estado']==1){
            echo "<a href='$eliminar'>Eliminar</a></td>";
            echo "<td>" .$libro['nombre']."</td>"
                ."<td>" .$libro['isbn']."</td>";
            echo "<td>Activo</td>";
        }elseif($libro['estado']==0){
            echo "<a href='$activar'>Activar</a></td>";
            echo "<td>" .$libro['nombre']."</td>"
                ."<td>" .$libro['isbn']."</td>";
            echo "<td>Inactivo</td>";
        }
                   
        echo "</tr>";
    }
    echo "</tbody>";
    echo "</table>";
?>


</div>
    
</body>
</html>


