<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pastel;

class PastelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->id_tematica > 0){
            $pasteles = Pastel::join('tematicas','pastels.id_tematica','=','tematicas.id')
            ->join('rellenos','pastels.id_relleno','=','rellenos.id')
            ->select('pastels.id','pastels.id_tematica','pastels.id_relleno','pastels.nombre',
            'tematicas.nombre as tematica',
            'rellenos.nombre as relleno',
            'pastels.precio',
            'pastels.descripcion','pastels.imagen','pastels.condicion')->where('pastels.id_tematica','=',$request->id_tematica)->get();
        }else{
            $pasteles = Pastel::join('tematicas','pastels.id_tematica','=','tematicas.id')
            ->join('rellenos','pastels.id_relleno','=','rellenos.id')
            ->select('pastels.id','pastels.id_tematica','pastels.id_relleno','pastels.nombre',
            'tematicas.nombre as tematica',
            'rellenos.nombre as relleno',
            'pastels.precio',
            'pastels.descripcion','pastels.imagen','pastels.condicion')->get();

        }
        
        return ['pasteles' => $pasteles];
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pastel = new Pastel();
        $pastel->nombre=$request->nombre;
        $pastel->id_relleno=$request->id_relleno;
        $pastel->descripcion=$request->descripcion;
        $pastel->precio=$request->precio;

        $tematica = $request->id_tematica;
        $pastel->id_tematica= $tematica;
        
        $file = $request->imagen;
        $filename = $file->getClientOriginalName();
        $path='pasteles/'.$tematica.'/';
        $file->move($path, $filename);
        $pastel->imagen=$path.$filename;
        
        $pastel->condicion='1';
        $pastel->save();
    }

    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
        $pastel = Pastel::findOrFail($request->id);
        $pastel->nombre=$request->nombre;
        $pastel->id_relleno=$request->id_relleno;
        $pastel->descripcion=$request->descripcion;
        $pastel->precio=$request->precio;

        $tematica = $request->id_tematica;
        $pastel->id_tematica= $tematica;
        
        $file = $request->imagen;
        $filename = $file->getClientOriginalName();
        $path='pasteles/'.$tematica.'/';
        $file->move($path, $filename);
        $pastel->imagen=$path.$filename;
        
        $pastel->condicion='1';
        $pastel->save();
        return $request->nombre;
    }

    public function desactivar(Request $request)
    {
        $pastel = Pastel::findOrFail($request->id);
        $pastel->condicion = '0';
        $pastel->save();
    }

    public function activar(Request $request)
    {
        $pastel = Pastel::findOrFail($request->id);
        $pastel->condicion = '1';
        $pastel->save();
    }


    
}
