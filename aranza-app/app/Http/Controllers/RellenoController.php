<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Relleno;

class RellenoController extends Controller
{
    public function index()
    {
        $Rellenos = Relleno::all();
        return $Rellenos;
    }

    public function selectRelleno(Request $request){
        $rellenos = Relleno::where('condicion','=','1')
        ->select('id','nombre')->orderBy('nombre','asc')->get();
        return ['rellenos' => $rellenos];
    }

    public function desactivar(Request $request)
    {
        $Relleno = Relleno::findOrFail($request->id);
        $Relleno->condicion = '0';
        $Relleno->save();
    }

    public function activar(Request $request)
    {
        $Relleno = Relleno::findOrFail($request->id);
        $Relleno->condicion = '1';
        $Relleno->save();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $Relleno = new Relleno();
        $Relleno->nombre=$request->nombre;
        $Relleno->descripcion=$request->descripcion;
        $Relleno->condicion = '1';
        $Relleno->save();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $Relleno = Relleno::findOrFail($request->id);
        $Relleno->nombre=$request->nombre;
        $Relleno->descripcion=$request->descripcion;
        $Relleno->condicion = '1';
        $Relleno->save();
    }
}
