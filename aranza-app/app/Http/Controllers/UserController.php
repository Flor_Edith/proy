<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function store(Request $request){
        $user = new User();
        $user->name = $request->name;
        $user->password =  bcrypt( $request->password);
        $user->email    = $request->email;
        $user->id_rol = '2';
        $user->save();
        return $user;
    }

}
