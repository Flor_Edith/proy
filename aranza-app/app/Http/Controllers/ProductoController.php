<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Producto;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       // $productos = Producto::all();
        if($request->id_categoria > 0){
            $productos = Producto::join('categorias','productos.id_categoria','=','categorias.id')
            ->select('productos.id','productos.id_categoria','productos.nombre',
            'categorias.nombre as categoria','productos.precio','productos.descripcion',
            'productos.condicion')->where('productos.id_categoria', '=', $request->id_categoria)->get();
        }else{
            $productos = Producto::join('categorias','productos.id_categoria','=','categorias.id')
            ->select('productos.id','productos.id_categoria','productos.nombre',
            'categorias.nombre as categoria','productos.precio','productos.descripcion',
            'productos.condicion')->get();
        }

       
            //->where('productos.'.$criterio, 'like', '%'. $buscar . '%')
            //->orderBy('productos.id', 'desc')->paginate(3);
        return ['productos' => $productos];
        
    }

   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $producto = new Producto();
        $producto->nombre = $request->nombre;
        $producto->descripcion = $request->descripcion;
        $producto->precio = $request->precio;
        $producto->condicion = '1';
        $producto->id_categoria = $request->id_categoria;
        $producto->save();
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $producto = Producto::findOrFail($request->id);
        $producto->nombre = $request->nombre;
        $producto->descripcion = $request->descripcion;
        $producto->precio = $request->precio;
        $producto->condicion = '1';
        $producto->id_categoria = $request->id_categoria;
        $producto->save();
    }

    public function desactivar(Request $request)
    {
        $producto = Producto::findOrFail($request->id);
        $producto->condicion = '0';
        $producto->save();
    }

    public function activar(Request $request)
    {
        $producto = Producto::findOrFail($request->id);
        $producto->condicion = '1';
        $producto->save();
    }

}
