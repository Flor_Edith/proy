<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tematica;

class TematicaController extends Controller
{
    public function index()
    {
        $Tematicas = Tematica::all();
        return $Tematicas;
    }

    public function selectTematica(Request $request){
        $tematicas = Tematica::where('condicion','=','1')
        ->select('id','nombre')->orderBy('nombre','asc')->get();
        return ['tematicas' => $tematicas];
    }

    public function desactivar(Request $request)
    {
        $Tematica = Tematica::findOrFail($request->id);
        $Tematica->condicion = '0';
        $Tematica->save();
    }

    public function activar(Request $request)
    {
        $Tematica = Tematica::findOrFail($request->id);
        $Tematica->condicion = '1';
        $Tematica->save();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        var_dump($request );
        $Tematica = new Tematica();
        $Tematica->nombre=$request->nombre;
        $Tematica->descripcion=$request->descripcion;
        $Tematica->condicion = '1';
        $Tematica->save();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $Tematica = Tematica::findOrFail($request->id);
        $Tematica->nombre=$request->nombre;
        $Tematica->descripcion=$request->descripcion;
        $Tematica->condicion = '1';
        $Tematica->save();
    }
}
