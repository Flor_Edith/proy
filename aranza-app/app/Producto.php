<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $fillable = ['id_categoria','nombre','descripcion','precio','condicion'];

    public function categoria(){
        return $this->belongsTo('App\Categoria');
    }
}
