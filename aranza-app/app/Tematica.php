<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tematica extends Model
{
    protected $fillable=['nombre','descripcion','condicion'];

    public function pastels(){
        return $this->hasMany('App\Pastel');
    }
}
