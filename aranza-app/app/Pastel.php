<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pastel extends Model
{
    protected $fillable = ['nombre','id_tematica','id_relleno','descripcion','precio','imagen','condicion'];

    public function tematica(){
        return $this->belongsTo('App\Tematica');
    }

    public function relleno(){
        return $this->belongsTo('App\Relleno');
    }
}
