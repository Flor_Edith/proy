let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

 mix.styles([
   'resources/assets/css/animate.css',
   'resources/assets/css/bootstrap.css',
   'resources/assets/css/bootstrap-datepicker.min.css',
   'resources/assets/css/bootstrap-datetimepicker.min.css',
   'resources/assets/css/flexslider.css',
   'resources/assets/css/icomoon.css',
   'resources/assets/css/magnific-popup.css',
   'resources/assets/css/owl.carousel.min.css',
   'resources/assets/css/owl.theme.default.min.css',
   'resources/assets/css/style.css',
   'resources/assets/css/themify-icons.css'
],'public/css/plantilla.css')
.scripts([
   'resources/assets/js/jquery.min.js',
   'resources/assets/js/jquery.countTo.js',
   'resources/assets/js/jquery.easing.1.3.js',
   'resources/assets/js/jquery.magnific-popup.min.js',
   
   'resources/assets/js/jquery.stellar.min.js',
   'resources/assets/js/jquery.waypoints.min.js',
   'resources/assets/js/magnific-popup-options.js',
   'resources/assets/js/main.js',
   'resources/assets/js/modernizr-2.6.2.min.js',
   'resources/assets/js/moment.min.js',
   'resources/assets/js/owl.carousel.min.js',
   'resources/assets/js/respond.min.js',
   'resources/assets/js/bootstrap.min.js',
   'resources/assets/js/bootstrap-datepicker.min.js',
   'resources/assets/js/bootstrap-datetimepicker.min.js',
   'resources/assets/js/sweetalert2.all.min.js'
],'public/js/plantilla.js')
.js('resources/assets/js/app.js', 'public/js/app.js');
