<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('contenido/contenido');
})->name('main');

Route::get('/categoria','CategoriaController@index');
Route::post('/categoria/registrar','CategoriaController@store');
Route::put('/categoria/actualizar','CategoriaController@update');

Route::put('/categoria/desactivar','CategoriaController@desactivar');
Route::put('/categoria/activar','CategoriaController@activar');
Route::post('/categoria/actualizar','CategoriaController@update');



Route::get('/categoria/selectCategoria','CategoriaController@selectCategoria');

Route::get('/producto','ProductoController@index');
Route::post('/producto/registrar','ProductoController@store');
Route::put('/producto/actualizar','ProductoController@update');

Route::put('/producto/desactivar','ProductoController@desactivar');
Route::put('/producto/activar','ProductoController@activar');



Route::get('/tematica','TematicaController@index');
Route::post('/tematica/registrar','TematicaController@store');
Route::put('/tematica/actualizar','TematicaController@update');
Route::put('/tematica/desactivar','TematicaController@desactivar');
Route::put('/tematica/activar','TematicaController@activar');
Route::post('/tematica/actualizar','TematicaController@update');

Route::get('/rol','RolController@index');

Route::get('/relleno','RellenoController@index');
Route::post('/relleno/registrar','RellenoController@store');
Route::put('/relleno/actualizar','RellenoController@update');
Route::put('/relleno/desactivar','RellenoController@desactivar');
Route::put('/relleno/activar','RellenoController@activar');
Route::post('/relleno/actualizar','RellenoController@update');

Route::get('/pastel','PastelController@index');
Route::post('/pastel/registrar','PastelController@store');
Route::put('/pastel/desactivar','PastelController@desactivar');
Route::put('/pastel/activar','PastelController@activar');
Route::post('/pastel/actualizar','PastelController@update');

Route::get('relleno/selectRelleno','RellenoController@selectRelleno');
Route::get('tematica/selectTematica','TematicaController@selectTematica');



Route::get('/rol','RolController@index');


Route::get('/home', 'HomeController@index')->name('home');

Route::get('/loginForm','Auth\LoginController@showLoginForm')->name('acceder');
Route::post('/login','Auth\LoginController@login')->name('login');

Route::post('/user/registrar','UserController@store');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
