<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePastelesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pastels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',50);
            $table->integer('id_relleno')->unsigned();
            $table->integer('id_tematica')->unsigned();
            $table->string('descripcion',256)->nullable();
            $table->float('precio',8,2)->nullable();
            $table->string('imagen',150)->nullable();
            $table->boolean('condicion')->default(1);
           

            $table->foreign('id_relleno')->references('id')->on('rellenos');
            $table->foreign('id_tematica')->references('id')->on('tematicas');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pasteles');
    }
}
