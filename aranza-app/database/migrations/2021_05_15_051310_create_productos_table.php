<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_categoria')->unsigned();      
            $table->string('nombre', 50);
            $table->string('descripcion',256)->nullable();
            $table->float('precio', 8, 2)->default(0.0);
            $table->boolean('condicion')->default(1);
            $table->timestamps();
                 
            $table->foreign('id_categoria')->references('id')->on('categorias');         
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
