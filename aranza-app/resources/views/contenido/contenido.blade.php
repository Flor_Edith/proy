@extends('principal')
@section('contenido')

<!-- menus del cliente o invitado -->
<template v-if="menu==0">
    <pastel-cliente :ruta="ruta"></pastel-cliente>
</template>

<template v-if="menu==8">
    <producto-cliente :ruta="ruta"></producto-cliente>
</template>

<template v-if="menu==9">
    
    <carrito :ruta="ruta"></carrito>
</template>


<!-- menus de administrador -->
<template v-if="menu==1">
    <pastel :ruta="ruta"></pastel>
</template>

<template v-if="menu==2">
    <producto :ruta="ruta"></producto>
</template>
<template v-if="menu==4">
    <tematica :ruta="ruta"></tematica>
</template>
<template v-if="menu==5">
    <relleno :ruta="ruta"></relleno>
</template>
<template v-if="menu==3">    
    <categoria :ruta="ruta"></categoria>
</template>

<template v-if="menu==6">    
    <rol></rol>
</template>

<!-- Menu para acceder -->
<template v-if="menu==7">    
    <user></user>
</template>



@endsection