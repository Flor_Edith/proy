@extends('auth.contenido')

@section('login')

<form class="form-horizontal was-validated" method="POST" action="{{ route('login')}}">
   {{ csrf_field() }}
    <div class="row form-group mb-3{{$errors->has('email' ? 'is-invalid' : '')}}">
        <div class="col-md-12">
            <label >Correo electrónico</label>
            <input  type="text" id="email" name="email" class="form-control">
            {!!$errors->first('email', '<span class="invalid-feedback" role="alert">:message</span>')!!}
            <label>Contraseña</label>
            <input type="password" id="password" name="password" class="form-control">
            {!!$errors->first('password', '<span class="invalid-feedback">:message</span>')!!}
            <div class="form-group row div-error ">
                <div class="text-center text-error">
                    <div></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-md-12">
            <!-- button -->
            <button type="submit" class="btn btn-primary">Acceder</button>
        </div>
            
    </div>
</form>	

@endsection
