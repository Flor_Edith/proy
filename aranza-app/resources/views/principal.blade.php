<!DOCTYPE HTML>

<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="shortcut icon" href="images/logos/logo_transparent.png" />
        <title>Pastelerías Aranza - La chiquita</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Free HTML5 Website Template by GetTemplates.co" />
        <meta name="keywords" content="free website templates, free html5, free template, free bootstrap, free website template, html5, css3, mobile first, responsive" />
        <meta name="author" content="GetTemplates.co" />

        <!-- Facebook and Twitter integration -->
        <meta property="og:title" content=""/>
        <meta property="og:image" content=""/>
        <meta property="og:url" content=""/>
        <meta property="og:site_name" content=""/>
        <meta property="og:description" content=""/>
        <meta name="twitter:title" content="" />
        <meta name="twitter:image" content="" />
        <meta name="twitter:url" content="" />
        <meta name="twitter:card" content="" />

        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
        
        <!-- Animate.css -->
        <link rel="stylesheet" href="css/plantilla.css">

        <!-- Modernizr JS -->
        <script src="js/modernizr-2.6.2.min.js"></script>
        <!-- FOR IE9 below -->
        <!--[if lt IE 9]>
        <script src="js/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
        <div id="app">
        <div class="gtco-loader"></div>
        <div id="page">

        <!-- aqui vamos a poner las condiciones de roll (cliente, administrador, invitado)-->
        <!-- para mostrar su barra de navegación -->
        @if(Auth::check())
            @if(Auth::user()->id_rol == 1)
                @include('plantilla.administrador')
            @elseif(Auth::user()->id_rol == 2)
                @include('plantilla.cliente')
            @endif
        @else
            @include('plantilla.invitado')
        @endif
        
        <header id="gtco-header" class="gtco-cover gtco-cover-xs" role="banner" style="background-image: url(images/back.png)" data-stellar-background-ratio="0.5">
            <div class="overlay"></div>
            <div class="gtco-container">
                <div class="row">
                    <div class="col-md-12 col-md-offset-0 text-left">
                        <div class="row row-mt-15em" align-text="center">
                            <h1 class="cursive-font">Pastelerías Aranza</h1>
                        </div>
                    </div>
                </div> 
            </div>
        </header>

        <div class="gtco-section">
		    <div class="gtco-container">



                @yield('contenido')

                

            </div>
	    </div>
        

        <footer id="gtco-footer" role="contentinfo" style="background-image: url(images/img_bg_1.jpg)" data-stellar-background-ratio="0.5">
            <div class="overlay"></div>
            <div class="gtco-container">
                <div class="row row-pb-md">
                    <div class="col-md-12 text-center">
                        <div class="gtco-widget">
                            <h3>Contacto</h3>
                            <ul class="gtco-quick-contact">
                                <li><a href="#"><i class="icon-phone"></i> +52 722 408 9581</a></li>
                                <li><a href="#"><i class="icon-mail2"></i> museck_3132@hotmail.com </a></li>
                            </ul>
                        </div>
                        <div class="gtco-widget">
                            <h3>Redes sociales</h3>
                            <ul class="gtco-social-icons">
                                <li><a href="#"><i class="icon-twitter"></i></a></li>
                                <li><a href="https://www.facebook.com/salvador3132"><i class="icon-facebook"></i></a></li>
                                <li><a href="#"><i class="icon-linkedin"></i></a></li>
                                <li><a href="#"><i class="icon-dribbble"></i></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-12 text-center copyright">
                        <p><small class="block">&copy; 2021 </small> 
                            <small class="block">Diseñador por: Salvador Aranza Gasca & Flor Edith Arriaga Espinosa</small></p>
                    </div>
                </div>
            </div>
        </footer>

        <div class="gototop js-top">
		    <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
        </div>
        
        
        </div>
        </div>
        <script src="js/app.js"></script>
        <script src="js/plantilla.js"></script>
    </body>

</html>