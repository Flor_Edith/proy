<nav class="gtco-nav" role="navigation">
    <div class="gtco-container">
        <div class="row">
            <div class="col-sm-4 col-xs-12">
                <div id="gtco-logo"><a @click="menu=0" href="#"> <img width="200px" src="images/logos/logo_transparent.png" alt=""></a></div>
            </div>
            <!-- menu del administrador -->
            <div class="col-xs-8 text-right menu-1">
                <ul>
                    <li class="has-dropdown">
                        <li class="has-dropdown">
                            <a @click="menu=1" href="#">Pasteles</a>
                            <ul class="dropdown">
                                <li><a @click="menu=5" href="#">Relleno</a></li>
                                <li><a @click="menu=4" href="#">Tematica</a></li>
                            </ul>
                        </li>
                        <li class="has-dropdown">
                            <a @click="menu=2" href="#">Producto</a>
                        </li>
                        <li><a  @click="menu=3" href="#">Categoría</a></li>
                        <li class="has-dropdown">
                            <a href="#">Administrador</a>
                            <ul class="dropdown">
                                <li><a  @click="menu=6" href="#">Roles</a></li>
                            </ul>
                        </li>
                    </li>                    
                </ul>	
            </div>
            <!-- menu del cliente -->
            
        </div>
    </div>
</nav>