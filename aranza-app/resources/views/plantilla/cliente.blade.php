<nav class="gtco-nav" role="navigation">
    <div class="gtco-container">
        <div class="row">
            <div class="col-sm-4 col-xs-12">
                <div id="gtco-logo"><a @click="menu=0" href="#"> <img width="200px" src="images/logos/logo_transparent.png" alt=""></a></div>
            </div>
            <!-- menu del administrador -->
            <div class="col-xs-8 text-right menu-1">
                <ul>
                    <li class="has-dropdown">
                        <li class="has-dropdown">
                            <a @click="menu=0" href="#">Pasteles</a>
                        </li>
                        <li class="has-dropdown">
                            <a @click="menu=8" href="#">Producto</a>
                        </li>
                        
                        <li class="has-dropdown">
                            <a href="#">{{Auth::user()->name}}</a>
                            <i class="ti-angle-down"></i>
                            <ul class="dropdown">
                                <li>
                                    <a class="dropdown-item" href="{{ route('logout') }}" 
                                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                     Cerrar sesión    <i class="ti-power-off"></i></a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    </li>                    
                </ul>	
            </div>
            <!-- menu del cliente -->
            
        </div>
    </div>
</nav>