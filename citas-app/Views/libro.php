<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registro Libro - Sistema de biblioteca en línea</title>
    <link rel="stylesheet" href="/citas-app/css/style.css">
    <link rel="stylesheet" href="/citas-app/css/normalize.css">
    <script type="text/javascript">
        function valideKey(evt){
            
            // code is the decimal ASCII representation of the pressed key.
            var code = (evt.which) ? evt.which : evt.keyCode;
            
            if(code==8) { // backspace.
            return true;
            } else if(code>=48 && code<=57) { // is a number.
            return true;
            } else{ // other keys.
            return false;
            }
        }
    </script> 
</head>
<body>

<div class="container">
<section>
    <h1 class="text-center">Registrar libro</h1>
        <div class="center">
    <form action="/citas-app/querys/RegistrarLibro.php" method="POST">

        <div class="form-group">
                <input type="text" name="nombre" class="form-control" placeholder="Nombre">
            </div>
            <div class="form-group">
                <input type="text" name="isbn" class="form-control" onkeypress="return valideKey(event);" placeholder="ISBN">
            </div>
            <div class="form-group">
                <input type="submit" class="form-control btn" value="Registrar" />
            </div> 
    </form>
    </div>
</div>
    
</body>
</html>
