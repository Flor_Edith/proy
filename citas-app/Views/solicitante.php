<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Solicitante - Sistema de biblioteca en línea</title>
    <link rel="stylesheet" href="/citas-app/css/style.css">
    <link rel="stylesheet" href="/citas-app/css/normalize.css">
    <script type="text/javascript">
        function check(){
            if(document.getElementById("nombre").value == '' || (document.getElementById("nombre").value == null)){
                alert("no");
                return false;
            }
                
            else
                return true;
            
        }
    </script> 
</head>
<body>

<ul>
            <div class="container">
            <li><a href="/citas-app/Views/registrar.php">Registrar Responsable</a></li>
            <li><a href="/citas-app/Views/login.php" >Iniciar sesión</a></li>
            <li><a class="active" href="/citas-app/index.php">Inicio</a></li>
            </div>
        </ul>
<div class="container">
    <section></section>
    <h1 class="text-center">Registrate</h1>
    <div class="center">

    
    <form action="/citas-app/querys/SolicitanteRegistroController.php"  onsubmit="return check();" method="POST">

        <div class="form-group">
                <input type="text" name="nombre" id="nombre" class="form-control" placeholder="Nombre">
            </div>
            <div class="form-group">
                <input type="text" name="telefono" class="form-control" placeholder="Telefono">
            </div>
            <div class="form-group">
                <input type="text" name="asunto" class="form-control" placeholder="Asunto">
            </div>
            <div class="form-group">
                <input type="date" name="fecha" class="form-control" placeholder="Fecha">
            </div>
            <div class="form-group">
                <input type="submit" class="form-control btn" onkeypress="return valideKey(event);" value="Registrar solicitante" />
            </div> 
    </form>
    </div>
</div>

    
</body>
</html>