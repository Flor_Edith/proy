<?php 
    session_start();
    if ( $_SESSION['usuario'] == null ||  $_SESSION['usuario'] == ''){
        echo "usted no tiene autorización";
        die();
    }
    require_once('../querys/ConsultarResponsablesController.php');
    //require_once('../querys/ConsultarSolicitantesController.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Citas - Sistema de biblioteca en línea</title>
    <link rel="stylesheet" href="/citas-app/css/style.css">
    <link rel="stylesheet" href="/citas-app/css/normalize.css">
    <script type="text/javascript">
        function check(){
            if(document.getElementById("nombre").value == '' || (document.getElementById("nombre").value == null)){
                alert("no");
                return false;
            }
                
            else
                return true;
            
        }
    </script> 
</head>
<body>

<ul>
    <div class="container">
    <li><a href="/citas-app/querys/CerrarSesion.php">Cerrar sesión</a></li>
    <li><a href="/citas-app/querys/GenerarReportePDF.php" target="_blank">Reporte PDF</a></li>
    <li><a href="/citas-app/querys/GenerarReporteExcel.php" target="_blank">Reporte Excel</a></li>
    <li><a href="/citas-app/views/citas.php">Generar cita</a></li>
    <li><a href="/citas-app/views/responsables.php">Registrate Responsable</a></li>
    <li><a href="/citas-app/Views/solicitante.php">Registrar Solicitante</a></li>
    <li><a class="active" href="/citas-app/Views/principal.php">Inicio</a></li>
    </div>
    </ul>
<div class="container">
    <section></section>
    <h1 class="text-center">Registrar cita</h1>
    <div class="center">

    
    <form action="/citas-app/querys/AgendarCitaController.php"  onsubmit="return check();" method="POST">

        <div class="form-group">
        <select class="form-control" name="responsable"> 
            <?php foreach ($responsables as $key => $responsable) {  ?>
                    <option value="<?php echo $responsable['id'] ?>"><?php echo $responsable['id'].' - '.$responsable['nombre'] ?> </option> "
            <?php    } ?>
             
        </select>
            </div>

            <div class="form-group">
        <select class="form-control" name="solicitante">    
        <?php foreach ($solicitantes as $key => $solicitante) {  ?>
                    <option value="<?php echo $solicitante['id'] ?>"><?php echo $solicitante['fecha'].' - '.$solicitante['nombre'] ?> </option> "
            <?php    } ?>
        </select>
        </div>

        <div class="form-group">
        <select class="form-control" name="hora">    
            <option value="09:00">09:00</option>
            <option value="09:30">09:30</option>
            <option value="10:00">10:00</option>
            <option value="10:30">10:30</option>
            <option value="11:00">11:00</option>
            <option value="11:30">11:30</option>
            <option value="12:00">12:00</option>
            <option value="12:30">12:30</option>
        </select>
        </div>
            
            <div class="form-group">
                <input type="submit" class="form-control btn" onkeypress="return valideKey(event);" value="Validar y Guardar" />
            </div> 
    </form>
    </div>
</div>

    
</body>
</html>