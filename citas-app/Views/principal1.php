<?php 

    session_start();
    if ( $_SESSION['usuario'] == null ||  $_SESSION['usuario'] == ''){
        echo "usted no tiene autorización";
        die();
    }
    require_once('../querys/ConsultarLibrosController.php');
?>
<h1>Principal</h1>
<h2>Bienvenido: <?php echo $_SESSION['usuario'] ?></h2>
<a href="/citas-app/querys/CerrarSesion.php">Cerrar sesión</a>

<h3>Usted puede registrar un libro hacindo click en:</h3>
<a href="/citas-app/Views/libro.php">Registrar Libro</a>

<a href="/citas-app/querys/GenerarReportePDF.php" target="_blank">Reporte PDF</a>

<?php
    echo "<table border=1>";
    echo "<thead>";
    echo "<tr>";
    echo "<td>Acción</td>";
    echo "<td>Nombre</td>";
    echo "<td>ISBN</td>";
    echo "<td>Estado</td>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    foreach ($libros as $libro){
        echo "<tr>";
        $url = "/citas-app/Views/ModificarLibro.php?id=".$libro['id']."&nombre=".$libro['nombre']."&isbn=".$libro['isbn'];
        $eliminar = "/citas-app/querys/EliminarLibroController.php?id=".$libro['id'];
        echo  "<td><a href='$url'>Modificar</a>"
             ."<a href='$eliminar'>Eliminar</a></td>"
             ."<td>" .$libro['nombre']."</td>"
             ."<td>" .$libro['isbn']."</td>";
        if($libro['estado']==1){
            echo "<td>Activo</td>";
        }elseif($libro['estado']==0){
            echo "<td>Inactivo</td>";
        }
                   
        echo "</tr>";
    }
    echo "</tbody>";
    echo "</table>";
?>