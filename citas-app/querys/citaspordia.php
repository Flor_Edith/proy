<?php require_once('conn.php'); ?>
<?php 

if ($conn) {
    $dia = $_POST['fecha']

    $sql       = "SELECT * FROM citas where fecha=$dia";

    $resultado = mysqli_query($conn, $sql);
    if (!$resultado) {
        echo "Error de BD, no se pudo consultar la base de datos\n";
        echo "Error MySQL: ". mysql_error();
        exit;
    }
    $citas = array();
    while ($fila = mysqli_fetch_assoc($resultado)) {
        $row = [
            'solicitante' => $fila['id_solicitante'],
            'responsable' => $fila['id_responsable'],
            'fecha' => $fila['fecha'],
            'hora' => $fila['hora']
        ];
        array_push($citas,$row);
    }
    
    mysqli_free_result($resultado);
}

mysqli_close($conn);

?>