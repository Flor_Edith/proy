<?php require_once('conn.php'); ?>
<?php 

if ($conn) {

    $sql       = "SELECT * FROM responsables";

    $resultado = mysqli_query($conn, $sql);
    if (!$resultado) {
        echo "Error de BD, no se pudo consultar la base de datos\n";
        echo "Error MySQL: ". mysql_error();
        exit;
    }
    $responsables = array();
    while ($fila = mysqli_fetch_assoc($resultado)) {
        $row = [
            'id' => $fila['id'],
            'nombre' => $fila['nombre'],
        ];
        array_push($responsables,$row);
    }
    
    mysqli_free_result($resultado);

    $sql       = "SELECT * FROM solicitantes";

    $resultado = mysqli_query($conn, $sql);
    if (!$resultado) {
        echo "Error de BD, no se pudo consultar la base de datos\n";
        echo "Error MySQL: ". mysql_error();
        exit;
    }
    $solicitantes = array();
    while ($fila = mysqli_fetch_assoc($resultado)) {
        $row = [
            'id' => $fila['id'],
            'nombre' => $fila['nombre'],
            'fecha' => $fila['fecha']
        ];
        array_push($solicitantes,$row);
    }
    
}

mysqli_close($conn);

?>